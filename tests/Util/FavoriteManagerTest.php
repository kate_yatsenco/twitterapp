<?php

namespace App\Tests\Util;

use App\Entity\Tweet;
use App\Entity\User;
use App\Util\FavoriteManager;
use PHPUnit\Framework\TestCase;

class FavoriteManagerTest extends TestCase
{
    public function testIsFavoriteTweet()
    {
        $tweet = new Tweet();
        $user = new User();
        $favoriteManager = new FavoriteManager();

        // Negative case
        $this->assertEquals(false, $favoriteManager->isFavoriteTweet($user, $tweet));

        // Add tweet to favorites
        $user->addFavoriteTweet($tweet);

        // Positive case
        $this->assertEquals(true, $favoriteManager->isFavoriteTweet($user, $tweet));
    }

    public function testIsFavoriteUser(){
        $currentUser = new User();
        $user = new User();
        $favoriteManager = new FavoriteManager();

        // Negative case
        $this->assertEquals(false, $favoriteManager->isFavoriteUser($currentUser, $user));

        // Add user to favorites
        $currentUser->addFavoriteUser($user);

        // Positive case
        $this->assertEquals(true, $favoriteManager->isFavoriteUser($currentUser, $user));
    }
}
