<?php

namespace App\Tests\Util;

use App\Entity\User;
use App\Util\SubscriptionManager;
use PHPUnit\Framework\TestCase;

class SubscriptionManagerTest extends TestCase
{
    public function test  () {
        $subscriber = new User();
        $subscriptionObject = new User();

        $subscriptionManager = new SubscriptionManager();

        // Negative case
        $this->assertEquals(false, $subscriptionManager->isSubscribedTo($subscriber, $subscriptionObject));

        // Add tweet to favorites
        $subscriber->follow($subscriber, $subscriptionObject);

        // Positive case
        $this->assertEquals(true, $subscriptionManager->isSubscribedTo($subscriber, $subscriptionObject));
    }
}
