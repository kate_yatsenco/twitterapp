<?php

namespace App\Tests\Util;

use App\Entity\Tweet;
use App\Entity\User;
use App\Util\LikeManager;
use PHPUnit\Framework\TestCase;

class LikeManagerTest extends TestCase
{
    public function testIsLikedTweet  () {
        $tweet = new Tweet();
        $user = new User();
        $likeManager = new LikeManager();

        // Negative case
        $this->assertEquals(false, $likeManager->isLikedTweet($user, $tweet));

        // Add tweet to favorites
        $user->addLikeToTweet($tweet);

        // Positive case
        $this->assertEquals(true, $likeManager->isLikedTweet($user, $tweet));
    }
}
