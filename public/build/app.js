(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/app.css":
/*!****************************!*\
  !*** ./assets/css/app.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./tweet/new.js */ "./assets/js/tweet/new.js");

__webpack_require__(/*! ./tweet/edit.js */ "./assets/js/tweet/edit.js");

__webpack_require__(/*! ./tweet/delete.js */ "./assets/js/tweet/delete.js");

__webpack_require__(/*! ./tweet/manageActions.js */ "./assets/js/tweet/manageActions.js");

__webpack_require__(/*! ./favorites/list.js */ "./assets/js/favorites/list.js");

__webpack_require__(/*! ./../css/app.css */ "./assets/css/app.css");

/***/ }),

/***/ "./assets/js/favorites/list.js":
/*!*************************************!*\
  !*** ./assets/js/favorites/list.js ***!
  \*************************************/
/*! exports provided: updateFavoritesByClick, updateFavorites */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateFavoritesByClick", function() { return updateFavoritesByClick; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateFavorites", function() { return updateFavorites; });
/* harmony import */ var _functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/render/renderDataFromUrlInsideBlock.js */ "./assets/js/functions/render/renderDataFromUrlInsideBlock.js");

updateFavoritesByClick();
function updateFavoritesByClick() {
  var showFavoriteListButton = document.getElementById('showFavoriteListButton'),
      favoriteListBody = document.getElementById('favoriteListBody');

  if (showFavoriteListButton) {
    showFavoriteListButton.addEventListener('click', function () {
      Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])('favorites', favoriteListBody);
    });
  }
}
function updateFavorites() {
  var showFavoriteListButton = document.getElementById('showFavoriteListButton'),
      favoriteListBody = document.getElementById('favoriteListBody');
  Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])('favorites', favoriteListBody);
}

/***/ }),

/***/ "./assets/js/functions/handle/tweetDelete.js":
/*!***************************************************!*\
  !*** ./assets/js/functions/handle/tweetDelete.js ***!
  \***************************************************/
/*! exports provided: handleTweetDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleTweetDelete", function() { return handleTweetDelete; });
function handleTweetDelete(tweetContentWrapper, tweetId) {
  new Promise(function (resolve, reject) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.addEventListener('load', function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          resolve();
        } else {
          reject(this.status);
        }
      }
    });
    httpRequest.open("DELETE", '/tweet/' + tweetId + '/delete');
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send();
  }).then(function () {
    tweetContentWrapper.parentElement.removeChild(tweetContentWrapper);
  }).catch(function (error) {
    console.error(error);
  });
}

/***/ }),

/***/ "./assets/js/functions/handle/tweetUpdate.js":
/*!***************************************************!*\
  !*** ./assets/js/functions/handle/tweetUpdate.js ***!
  \***************************************************/
/*! exports provided: handleTweetUpdate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleTweetUpdate", function() { return handleTweetUpdate; });
function handleTweetUpdate(form, tweetContentWrapper, tweetId) {
  form.addEventListener('submit', function (event) {
    event.preventDefault();
    new Promise(function (resolve, reject) {
      var httpRequest = new XMLHttpRequest(),
          formData = new FormData(form);
      httpRequest.addEventListener('load', function () {
        if (this.readyState === 4) {
          if (this.status === 200) {
            resolve(JSON.parse(this.response));
          } else {
            reject(this.status);
          }
        }
      });
      httpRequest.open("POST", '/tweet/' + tweetId + '/edit');
      httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      httpRequest.send(formData);
    }).then(function (data) {
      tweetContentWrapper.innerHTML = data;
    }).catch(function (error) {
      console.error(error);
    });
  });
}

/***/ }),

/***/ "./assets/js/functions/render/renderDataFromUrlInsideBlock.js":
/*!********************************************************************!*\
  !*** ./assets/js/functions/render/renderDataFromUrlInsideBlock.js ***!
  \********************************************************************/
/*! exports provided: renderDataFromUrlInsideBlock */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderDataFromUrlInsideBlock", function() { return renderDataFromUrlInsideBlock; });
function renderDataFromUrlInsideBlock(url, tweetWrapper) {
  var buttonNodeList = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  new Promise(function (resolve, reject) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.addEventListener('load', function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          resolve(JSON.parse(this.response));
        } else {
          reject(this.status);
        }
      }
    });
    httpRequest.open("GET", url);
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send();
  }).then(function (data) {
    if (buttonNodeList) {
      for (var i = 0; i < buttonNodeList.length; i++) {
        buttonNodeList[i].parentElement.innerHTML = data;
      }
    } else {
      tweetWrapper.innerHTML = data;
    }
  }).catch(function (error) {
    console.error(error);
  });
}

/***/ }),

/***/ "./assets/js/functions/render/tweetEditForm.js":
/*!*****************************************************!*\
  !*** ./assets/js/functions/render/tweetEditForm.js ***!
  \*****************************************************/
/*! exports provided: renderTweetEditForm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderTweetEditForm", function() { return renderTweetEditForm; });
function renderTweetEditForm(tweetId, tweetWrapper) {
  new Promise(function (resolve, reject) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.addEventListener('load', function () {
      if (this.readyState === 4) {
        if (this.status === 200) {
          resolve(JSON.parse(this.response));
        } else {
          reject(this.status);
        }
      }
    });
    httpRequest.open("GET", '/tweet/' + tweetId + '/edit');
    httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    httpRequest.send();
  }).then(function (data) {
    tweetWrapper.innerHTML = data;
  }).catch(function (error) {
    console.error(error);
  });
}

/***/ }),

/***/ "./assets/js/tweet/delete.js":
/*!***********************************!*\
  !*** ./assets/js/tweet/delete.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functions_handle_tweetDelete_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/handle/tweetDelete.js */ "./assets/js/functions/handle/tweetDelete.js");
// Function to handle tweet delete

document.addEventListener('click', function (event) {
  if (event.target.classList.contains('delete-tweet-button')) {
    var tweetWrapper = event.target.parentElement.parentElement,
        tweetId = tweetWrapper.dataset.tweetId;
    Object(_functions_handle_tweetDelete_js__WEBPACK_IMPORTED_MODULE_0__["handleTweetDelete"])(tweetWrapper, tweetId);
  }
});

/***/ }),

/***/ "./assets/js/tweet/edit.js":
/*!*********************************!*\
  !*** ./assets/js/tweet/edit.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functions_handle_tweetUpdate_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/handle/tweetUpdate.js */ "./assets/js/functions/handle/tweetUpdate.js");
/* harmony import */ var _functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../functions/render/renderDataFromUrlInsideBlock.js */ "./assets/js/functions/render/renderDataFromUrlInsideBlock.js");
/* harmony import */ var _functions_render_tweetEditForm_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../functions/render/tweetEditForm.js */ "./assets/js/functions/render/tweetEditForm.js");
// Function to handle tweet update
 // Additional functions to render templates



document.addEventListener('click', function (event) {
  if (event.target.classList.contains('edit-tweet-button')) {
    var tweetWrapper = event.target.parentElement.parentElement,
        tweetId = tweetWrapper.dataset.tweetId;
    Object(_functions_render_tweetEditForm_js__WEBPACK_IMPORTED_MODULE_2__["renderTweetEditForm"])(tweetId, tweetWrapper);
  } else if (event.target.id == 'close_editing_tweet') {
    var _tweetWrapper = event.target.parentElement.parentElement,
        _tweetId = _tweetWrapper.dataset.tweetId,
        url = '/tweet/' + _tweetId + '/show';
    Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_1__["renderDataFromUrlInsideBlock"])(url, _tweetWrapper);
  } else if (event.target.classList.contains('edited_tweet_save')) {
    var _tweetWrapper2 = event.target.parentElement.parentElement.parentElement,
        form = document.getElementById('editTweetForm'),
        _tweetId2 = _tweetWrapper2.dataset.tweetId;
    Object(_functions_handle_tweetUpdate_js__WEBPACK_IMPORTED_MODULE_0__["handleTweetUpdate"])(form, _tweetWrapper2, _tweetId2);
  }
});

/***/ }),

/***/ "./assets/js/tweet/manageActions.js":
/*!******************************************!*\
  !*** ./assets/js/tweet/manageActions.js ***!
  \******************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/render/renderDataFromUrlInsideBlock.js */ "./assets/js/functions/render/renderDataFromUrlInsideBlock.js");
/* harmony import */ var _favorites_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../favorites/list */ "./assets/js/favorites/list.js");


document.addEventListener('click', function (event) {
  // Manage user subscriptions
  if (event.target.classList.contains('follow-user-button')) {
    var followUserWrapper = event.target.parentElement,
        userId = event.target.dataset.userId,
        dataAttribute = '[data-user-id="' + userId + '"]',
        buttonNodeList = document.querySelectorAll(dataAttribute),
        url = '/follow/user/' + userId;
    Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])(url, followUserWrapper, buttonNodeList);
    Object(_favorites_list__WEBPACK_IMPORTED_MODULE_1__["updateFavorites"])();
  } // Manage tweet likes


  if (event.target.classList.contains('like-tweet-button')) {
    var favoriteTweetWrapper = event.target.parentElement,
        tweetId = event.target.dataset.tweetId,
        _url = '/manage/like/tweet/' + tweetId;

    Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])(_url, favoriteTweetWrapper);
    Object(_favorites_list__WEBPACK_IMPORTED_MODULE_1__["updateFavorites"])();
  } // Manage favorite tweets


  if (event.target.classList.contains('favorite-tweet-button')) {
    var _favoriteTweetWrapper = event.target.parentElement,
        _tweetId = event.target.dataset.tweetId,
        _url2 = '/manage/favorite/tweet/' + _tweetId;

    Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])(_url2, _favoriteTweetWrapper);
    Object(_favorites_list__WEBPACK_IMPORTED_MODULE_1__["updateFavorites"])();
  } // Manage favorite users


  if (event.target.classList.contains('favorite-user-button')) {
    var _favoriteTweetWrapper2 = event.target.parentElement,
        favoriteUserId = event.target.dataset.favoriteUserId,
        _dataAttribute = '[data-favorite-user-id="' + favoriteUserId + '"]',
        _buttonNodeList = document.querySelectorAll(_dataAttribute),
        _url3 = '/manage/favorite/user/' + favoriteUserId;

    Object(_functions_render_renderDataFromUrlInsideBlock_js__WEBPACK_IMPORTED_MODULE_0__["renderDataFromUrlInsideBlock"])(_url3, _favoriteTweetWrapper2, _buttonNodeList);
    Object(_favorites_list__WEBPACK_IMPORTED_MODULE_1__["updateFavorites"])();
  }
});

/***/ }),

/***/ "./assets/js/tweet/new.js":
/*!********************************!*\
  !*** ./assets/js/tweet/new.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var newTweetForm = document.getElementById('newTweetForm'),
    tweetSave = document.getElementById('tweet_save'),
    myTweetsContainer = document.getElementById('my-tweets-wrapper');

if (newTweetForm) {
  newTweetForm.addEventListener('submit', function (event) {
    event.preventDefault();
    new Promise(function (resolve, reject) {
      var httpRequest = new XMLHttpRequest(),
          formData = new FormData(newTweetForm);
      httpRequest.addEventListener('load', function () {
        if (this.readyState === 4) {
          if (this.status === 200) {
            resolve(JSON.parse(this.response));
          } else {
            reject(this.status);
          }
        }
      });
      httpRequest.open("POST", '/tweet/new');
      httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      httpRequest.send(formData);
    }).then(function (data) {
      //insert new Tweet as a last element of my tweets list
      myTweetsContainer.insertAdjacentHTML('beforeend', getNewTweetTemplate(data));
      tweetSave.blur();
      newTweetForm.reset();
    }).catch(function (error) {
      console.error(error);
    });
  });
} // get html for new tweet block


function getNewTweetTemplate(data) {
  return "<div class='card m-3 p-1' data-tweet-id='" + data.id + "'>" + "<p class='tweet-body'>" + data.name + "</p>" + "<div class='d-flex'>" + "<button class='edit-tweet-button btn btn-sm btn-outline-info m-1'>Edit</button>" + "<button class='delete-tweet-button btn btn-sm btn-outline-danger m-1'>Delete</button>" + "<span class='mt-1 mb-1 ml-auto mr-1'>" + data.user.username + "</span>" + "</div>" + "</div>";
}

/***/ })

},[["./assets/js/app.js","runtime"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvZmF2b3JpdGVzL2xpc3QuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Z1bmN0aW9ucy9oYW5kbGUvdHdlZXREZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Z1bmN0aW9ucy9oYW5kbGUvdHdlZXRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2Z1bmN0aW9ucy9yZW5kZXIvcmVuZGVyRGF0YUZyb21VcmxJbnNpZGVCbG9jay5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvZnVuY3Rpb25zL3JlbmRlci90d2VldEVkaXRGb3JtLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy90d2VldC9kZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3R3ZWV0L2VkaXQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3R3ZWV0L21hbmFnZUFjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3R3ZWV0L25ldy5qcyJdLCJuYW1lcyI6WyJyZXF1aXJlIiwidXBkYXRlRmF2b3JpdGVzQnlDbGljayIsInNob3dGYXZvcml0ZUxpc3RCdXR0b24iLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwiZmF2b3JpdGVMaXN0Qm9keSIsImFkZEV2ZW50TGlzdGVuZXIiLCJyZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrIiwidXBkYXRlRmF2b3JpdGVzIiwiaGFuZGxlVHdlZXREZWxldGUiLCJ0d2VldENvbnRlbnRXcmFwcGVyIiwidHdlZXRJZCIsIlByb21pc2UiLCJyZXNvbHZlIiwicmVqZWN0IiwiaHR0cFJlcXVlc3QiLCJYTUxIdHRwUmVxdWVzdCIsInJlYWR5U3RhdGUiLCJzdGF0dXMiLCJvcGVuIiwic2V0UmVxdWVzdEhlYWRlciIsInNlbmQiLCJ0aGVuIiwicGFyZW50RWxlbWVudCIsInJlbW92ZUNoaWxkIiwiY2F0Y2giLCJlcnJvciIsImNvbnNvbGUiLCJoYW5kbGVUd2VldFVwZGF0ZSIsImZvcm0iLCJldmVudCIsInByZXZlbnREZWZhdWx0IiwiZm9ybURhdGEiLCJGb3JtRGF0YSIsIkpTT04iLCJwYXJzZSIsInJlc3BvbnNlIiwiZGF0YSIsImlubmVySFRNTCIsInVybCIsInR3ZWV0V3JhcHBlciIsImJ1dHRvbk5vZGVMaXN0IiwiaSIsImxlbmd0aCIsInJlbmRlclR3ZWV0RWRpdEZvcm0iLCJ0YXJnZXQiLCJjbGFzc0xpc3QiLCJjb250YWlucyIsImRhdGFzZXQiLCJpZCIsImZvbGxvd1VzZXJXcmFwcGVyIiwidXNlcklkIiwiZGF0YUF0dHJpYnV0ZSIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJmYXZvcml0ZVR3ZWV0V3JhcHBlciIsImZhdm9yaXRlVXNlcklkIiwibmV3VHdlZXRGb3JtIiwidHdlZXRTYXZlIiwibXlUd2VldHNDb250YWluZXIiLCJpbnNlcnRBZGphY2VudEhUTUwiLCJnZXROZXdUd2VldFRlbXBsYXRlIiwiYmx1ciIsInJlc2V0IiwibmFtZSIsInVzZXIiLCJ1c2VybmFtZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7O0FDQUFBLG1CQUFPLENBQUMsZ0RBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxrREFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLHNEQUFELENBQVA7O0FBRUFBLG1CQUFPLENBQUMsb0VBQUQsQ0FBUDs7QUFFQUEsbUJBQU8sQ0FBQywwREFBRCxDQUFQOztBQUdBQSxtQkFBTyxDQUFDLDhDQUFELENBQVAsQzs7Ozs7Ozs7Ozs7O0FDVEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBQyxzQkFBc0I7QUFFZixTQUFTQSxzQkFBVCxHQUFrQztBQUNyQyxNQUFJQyxzQkFBc0IsR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLHdCQUF4QixDQUE3QjtBQUFBLE1BQ0lDLGdCQUFnQixHQUFHRixRQUFRLENBQUNDLGNBQVQsQ0FBd0Isa0JBQXhCLENBRHZCOztBQUdBLE1BQUlGLHNCQUFKLEVBQTRCO0FBQ3hCQSwwQkFBc0IsQ0FBQ0ksZ0JBQXZCLENBQXdDLE9BQXhDLEVBQWlELFlBQVk7QUFDekRDLDRIQUE0QixDQUFDLFdBQUQsRUFBY0YsZ0JBQWQsQ0FBNUI7QUFDSCxLQUZEO0FBR0g7QUFDSjtBQUVNLFNBQVNHLGVBQVQsR0FBMkI7QUFDOUIsTUFBSU4sc0JBQXNCLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3Qix3QkFBeEIsQ0FBN0I7QUFBQSxNQUNJQyxnQkFBZ0IsR0FBR0YsUUFBUSxDQUFDQyxjQUFULENBQXdCLGtCQUF4QixDQUR2QjtBQUdBRyx3SEFBNEIsQ0FBQyxXQUFELEVBQWNGLGdCQUFkLENBQTVCO0FBQ0gsQzs7Ozs7Ozs7Ozs7O0FDcEJEO0FBQUE7QUFBTyxTQUFTSSxpQkFBVCxDQUEyQkMsbUJBQTNCLEVBQWdEQyxPQUFoRCxFQUF5RDtBQUU1RCxNQUFJQyxPQUFKLENBQVksVUFBVUMsT0FBVixFQUFtQkMsTUFBbkIsRUFBMkI7QUFDbkMsUUFBSUMsV0FBVyxHQUFHLElBQUlDLGNBQUosRUFBbEI7QUFFQUQsZUFBVyxDQUFDVCxnQkFBWixDQUE2QixNQUE3QixFQUFxQyxZQUFZO0FBQzdDLFVBQUksS0FBS1csVUFBTCxLQUFvQixDQUF4QixFQUEyQjtBQUN2QixZQUFJLEtBQUtDLE1BQUwsS0FBZ0IsR0FBcEIsRUFBeUI7QUFDckJMLGlCQUFPO0FBQ1YsU0FGRCxNQUVPO0FBQ0hDLGdCQUFNLENBQUMsS0FBS0ksTUFBTixDQUFOO0FBQ0g7QUFDSjtBQUNKLEtBUkQ7QUFTQUgsZUFBVyxDQUFDSSxJQUFaLENBQWlCLFFBQWpCLEVBQTJCLFlBQVlSLE9BQVosR0FBc0IsU0FBakQ7QUFDQUksZUFBVyxDQUFDSyxnQkFBWixDQUE2QixrQkFBN0IsRUFBaUQsZ0JBQWpEO0FBRUFMLGVBQVcsQ0FBQ00sSUFBWjtBQUNILEdBaEJELEVBaUJLQyxJQWpCTCxDQWlCVSxZQUFNO0FBQ1JaLHVCQUFtQixDQUFDYSxhQUFwQixDQUFrQ0MsV0FBbEMsQ0FBOENkLG1CQUE5QztBQUNILEdBbkJMLEVBb0JLZSxLQXBCTCxDQW9CVyxVQUFDQyxLQUFELEVBQVc7QUFDZEMsV0FBTyxDQUFDRCxLQUFSLENBQWNBLEtBQWQ7QUFDSCxHQXRCTDtBQXVCSCxDOzs7Ozs7Ozs7Ozs7QUN6QkQ7QUFBQTtBQUFPLFNBQVNFLGlCQUFULENBQTJCQyxJQUEzQixFQUFpQ25CLG1CQUFqQyxFQUFzREMsT0FBdEQsRUFBK0Q7QUFDbEVrQixNQUFJLENBQUN2QixnQkFBTCxDQUFzQixRQUF0QixFQUFnQyxVQUFVd0IsS0FBVixFQUFpQjtBQUM3Q0EsU0FBSyxDQUFDQyxjQUFOO0FBQ0EsUUFBSW5CLE9BQUosQ0FBWSxVQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixFQUEyQjtBQUNuQyxVQUFJQyxXQUFXLEdBQUcsSUFBSUMsY0FBSixFQUFsQjtBQUFBLFVBQ0lnQixRQUFRLEdBQUcsSUFBSUMsUUFBSixDQUFhSixJQUFiLENBRGY7QUFHQWQsaUJBQVcsQ0FBQ1QsZ0JBQVosQ0FBNkIsTUFBN0IsRUFBcUMsWUFBWTtBQUM3QyxZQUFJLEtBQUtXLFVBQUwsS0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsY0FBSSxLQUFLQyxNQUFMLEtBQWdCLEdBQXBCLEVBQXlCO0FBQ3JCTCxtQkFBTyxDQUFDcUIsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsUUFBaEIsQ0FBRCxDQUFQO0FBQ0gsV0FGRCxNQUVPO0FBQ0h0QixrQkFBTSxDQUFDLEtBQUtJLE1BQU4sQ0FBTjtBQUNIO0FBQ0o7QUFDSixPQVJEO0FBU0FILGlCQUFXLENBQUNJLElBQVosQ0FBaUIsTUFBakIsRUFBeUIsWUFBWVIsT0FBWixHQUFzQixPQUEvQztBQUNBSSxpQkFBVyxDQUFDSyxnQkFBWixDQUE2QixrQkFBN0IsRUFBaUQsZ0JBQWpEO0FBRUFMLGlCQUFXLENBQUNNLElBQVosQ0FBaUJXLFFBQWpCO0FBQ0gsS0FqQkQsRUFrQktWLElBbEJMLENBa0JVLFVBQUNlLElBQUQsRUFBVTtBQUNaM0IseUJBQW1CLENBQUM0QixTQUFwQixHQUFnQ0QsSUFBaEM7QUFDSCxLQXBCTCxFQXFCS1osS0FyQkwsQ0FxQlcsVUFBQ0MsS0FBRCxFQUFXO0FBQ2RDLGFBQU8sQ0FBQ0QsS0FBUixDQUFjQSxLQUFkO0FBQ0gsS0F2Qkw7QUF3QkgsR0ExQkQ7QUEyQkgsQzs7Ozs7Ozs7Ozs7O0FDNUJEO0FBQUE7QUFBTyxTQUFTbkIsNEJBQVQsQ0FBc0NnQyxHQUF0QyxFQUEyQ0MsWUFBM0MsRUFBZ0Y7QUFBQSxNQUF2QkMsY0FBdUIsdUVBQU4sSUFBTTtBQUNuRixNQUFJN0IsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQ25DLFFBQUlDLFdBQVcsR0FBRyxJQUFJQyxjQUFKLEVBQWxCO0FBRUFELGVBQVcsQ0FBQ1QsZ0JBQVosQ0FBNkIsTUFBN0IsRUFBcUMsWUFBWTtBQUM3QyxVQUFJLEtBQUtXLFVBQUwsS0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsWUFBSSxLQUFLQyxNQUFMLEtBQWdCLEdBQXBCLEVBQXlCO0FBQ3JCTCxpQkFBTyxDQUFDcUIsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsUUFBaEIsQ0FBRCxDQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0h0QixnQkFBTSxDQUFDLEtBQUtJLE1BQU4sQ0FBTjtBQUNIO0FBQ0o7QUFDSixLQVJEO0FBU0FILGVBQVcsQ0FBQ0ksSUFBWixDQUFpQixLQUFqQixFQUF3Qm9CLEdBQXhCO0FBQ0F4QixlQUFXLENBQUNLLGdCQUFaLENBQTZCLGtCQUE3QixFQUFpRCxnQkFBakQ7QUFDQUwsZUFBVyxDQUFDTSxJQUFaO0FBQ0gsR0FmRCxFQWdCS0MsSUFoQkwsQ0FnQlUsVUFBQ2UsSUFBRCxFQUFVO0FBQ1osUUFBSUksY0FBSixFQUFvQjtBQUNoQixXQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdELGNBQWMsQ0FBQ0UsTUFBbkMsRUFBMkNELENBQUMsRUFBNUMsRUFBZ0Q7QUFDNUNELHNCQUFjLENBQUNDLENBQUQsQ0FBZCxDQUFrQm5CLGFBQWxCLENBQWdDZSxTQUFoQyxHQUE0Q0QsSUFBNUM7QUFDSDtBQUNKLEtBSkQsTUFJTztBQUNIRyxrQkFBWSxDQUFDRixTQUFiLEdBQXlCRCxJQUF6QjtBQUNIO0FBQ0osR0F4QkwsRUF5QktaLEtBekJMLENBeUJXLFVBQUNDLEtBQUQsRUFBVztBQUNkQyxXQUFPLENBQUNELEtBQVIsQ0FBY0EsS0FBZDtBQUNILEdBM0JMO0FBNEJILEM7Ozs7Ozs7Ozs7OztBQzdCRDtBQUFBO0FBQU8sU0FBU2tCLG1CQUFULENBQTZCakMsT0FBN0IsRUFBc0M2QixZQUF0QyxFQUFvRDtBQUN2RCxNQUFJNUIsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQ25DLFFBQUlDLFdBQVcsR0FBRyxJQUFJQyxjQUFKLEVBQWxCO0FBRUFELGVBQVcsQ0FBQ1QsZ0JBQVosQ0FBNkIsTUFBN0IsRUFBcUMsWUFBWTtBQUM3QyxVQUFJLEtBQUtXLFVBQUwsS0FBb0IsQ0FBeEIsRUFBMkI7QUFDdkIsWUFBSSxLQUFLQyxNQUFMLEtBQWdCLEdBQXBCLEVBQXlCO0FBQ3JCTCxpQkFBTyxDQUFDcUIsSUFBSSxDQUFDQyxLQUFMLENBQVcsS0FBS0MsUUFBaEIsQ0FBRCxDQUFQO0FBQ0gsU0FGRCxNQUVPO0FBQ0h0QixnQkFBTSxDQUFDLEtBQUtJLE1BQU4sQ0FBTjtBQUNIO0FBQ0o7QUFDSixLQVJEO0FBU0FILGVBQVcsQ0FBQ0ksSUFBWixDQUFpQixLQUFqQixFQUF3QixZQUFZUixPQUFaLEdBQXNCLE9BQTlDO0FBQ0FJLGVBQVcsQ0FBQ0ssZ0JBQVosQ0FBNkIsa0JBQTdCLEVBQWlELGdCQUFqRDtBQUNBTCxlQUFXLENBQUNNLElBQVo7QUFDSCxHQWZELEVBZ0JLQyxJQWhCTCxDQWdCVSxVQUFDZSxJQUFELEVBQVU7QUFDWkcsZ0JBQVksQ0FBQ0YsU0FBYixHQUF5QkQsSUFBekI7QUFDSCxHQWxCTCxFQW1CS1osS0FuQkwsQ0FtQlcsVUFBQ0MsS0FBRCxFQUFXO0FBQ2RDLFdBQU8sQ0FBQ0QsS0FBUixDQUFjQSxLQUFkO0FBQ0gsR0FyQkw7QUFzQkgsQzs7Ozs7Ozs7Ozs7O0FDdkJEO0FBQUE7QUFBQTtBQUNBO0FBRUF2QixRQUFRLENBQUNHLGdCQUFULENBQTBCLE9BQTFCLEVBQW1DLFVBQVV3QixLQUFWLEVBQWlCO0FBRWhELE1BQUlBLEtBQUssQ0FBQ2UsTUFBTixDQUFhQyxTQUFiLENBQXVCQyxRQUF2QixDQUFnQyxxQkFBaEMsQ0FBSixFQUE0RDtBQUV4RCxRQUFJUCxZQUFZLEdBQUdWLEtBQUssQ0FBQ2UsTUFBTixDQUFhdEIsYUFBYixDQUEyQkEsYUFBOUM7QUFBQSxRQUNJWixPQUFPLEdBQUc2QixZQUFZLENBQUNRLE9BQWIsQ0FBcUJyQyxPQURuQztBQUVBRiw4RkFBaUIsQ0FBQytCLFlBQUQsRUFBZTdCLE9BQWYsQ0FBakI7QUFDSDtBQUNKLENBUkQsRTs7Ozs7Ozs7Ozs7O0FDSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtDQUVBOztBQUNBO0FBQ0E7QUFFQVIsUUFBUSxDQUFDRyxnQkFBVCxDQUEwQixPQUExQixFQUFtQyxVQUFVd0IsS0FBVixFQUFpQjtBQUVoRCxNQUFJQSxLQUFLLENBQUNlLE1BQU4sQ0FBYUMsU0FBYixDQUF1QkMsUUFBdkIsQ0FBZ0MsbUJBQWhDLENBQUosRUFBMEQ7QUFFdEQsUUFBSVAsWUFBWSxHQUFHVixLQUFLLENBQUNlLE1BQU4sQ0FBYXRCLGFBQWIsQ0FBMkJBLGFBQTlDO0FBQUEsUUFDSVosT0FBTyxHQUFHNkIsWUFBWSxDQUFDUSxPQUFiLENBQXFCckMsT0FEbkM7QUFHQWlDLGtHQUFtQixDQUFDakMsT0FBRCxFQUFVNkIsWUFBVixDQUFuQjtBQUVILEdBUEQsTUFPTyxJQUFJVixLQUFLLENBQUNlLE1BQU4sQ0FBYUksRUFBYixJQUFtQixxQkFBdkIsRUFBOEM7QUFFakQsUUFBSVQsYUFBWSxHQUFHVixLQUFLLENBQUNlLE1BQU4sQ0FBYXRCLGFBQWIsQ0FBMkJBLGFBQTlDO0FBQUEsUUFDSVosUUFBTyxHQUFHNkIsYUFBWSxDQUFDUSxPQUFiLENBQXFCckMsT0FEbkM7QUFBQSxRQUVJNEIsR0FBRyxHQUFHLFlBQVk1QixRQUFaLEdBQXNCLE9BRmhDO0FBSUFKLDBIQUE0QixDQUFDZ0MsR0FBRCxFQUFNQyxhQUFOLENBQTVCO0FBRUgsR0FSTSxNQVFBLElBQUlWLEtBQUssQ0FBQ2UsTUFBTixDQUFhQyxTQUFiLENBQXVCQyxRQUF2QixDQUFnQyxtQkFBaEMsQ0FBSixFQUEwRDtBQUU3RCxRQUFJUCxjQUFZLEdBQUdWLEtBQUssQ0FBQ2UsTUFBTixDQUFhdEIsYUFBYixDQUEyQkEsYUFBM0IsQ0FBeUNBLGFBQTVEO0FBQUEsUUFDSU0sSUFBSSxHQUFHMUIsUUFBUSxDQUFDQyxjQUFULENBQXdCLGVBQXhCLENBRFg7QUFBQSxRQUVJTyxTQUFPLEdBQUc2QixjQUFZLENBQUNRLE9BQWIsQ0FBcUJyQyxPQUZuQztBQUlBaUIsOEZBQWlCLENBQUNDLElBQUQsRUFBT1csY0FBUCxFQUFxQjdCLFNBQXJCLENBQWpCO0FBQ0g7QUFDSixDQXpCRCxFOzs7Ozs7Ozs7Ozs7QUNOQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUFSLFFBQVEsQ0FBQ0csZ0JBQVQsQ0FBMEIsT0FBMUIsRUFBbUMsVUFBVXdCLEtBQVYsRUFBaUI7QUFFaEQ7QUFDQSxNQUFJQSxLQUFLLENBQUNlLE1BQU4sQ0FBYUMsU0FBYixDQUF1QkMsUUFBdkIsQ0FBZ0Msb0JBQWhDLENBQUosRUFBMkQ7QUFDdkQsUUFBSUcsaUJBQWlCLEdBQUdwQixLQUFLLENBQUNlLE1BQU4sQ0FBYXRCLGFBQXJDO0FBQUEsUUFDSTRCLE1BQU0sR0FBR3JCLEtBQUssQ0FBQ2UsTUFBTixDQUFhRyxPQUFiLENBQXFCRyxNQURsQztBQUFBLFFBRUlDLGFBQWEsR0FBRyxvQkFBb0JELE1BQXBCLEdBQTZCLElBRmpEO0FBQUEsUUFHSVYsY0FBYyxHQUFHdEMsUUFBUSxDQUFDa0QsZ0JBQVQsQ0FBMEJELGFBQTFCLENBSHJCO0FBQUEsUUFJSWIsR0FBRyxHQUFHLGtCQUFrQlksTUFKNUI7QUFNQTVDLDBIQUE0QixDQUFDZ0MsR0FBRCxFQUFNVyxpQkFBTixFQUF5QlQsY0FBekIsQ0FBNUI7QUFDQWpDLDJFQUFlO0FBQ2xCLEdBWitDLENBY2hEOzs7QUFDQSxNQUFJc0IsS0FBSyxDQUFDZSxNQUFOLENBQWFDLFNBQWIsQ0FBdUJDLFFBQXZCLENBQWdDLG1CQUFoQyxDQUFKLEVBQTBEO0FBQ3RELFFBQUlPLG9CQUFvQixHQUFHeEIsS0FBSyxDQUFDZSxNQUFOLENBQWF0QixhQUF4QztBQUFBLFFBQ0laLE9BQU8sR0FBR21CLEtBQUssQ0FBQ2UsTUFBTixDQUFhRyxPQUFiLENBQXFCckMsT0FEbkM7QUFBQSxRQUVJNEIsSUFBRyxHQUFHLHdCQUF3QjVCLE9BRmxDOztBQUlBSiwwSEFBNEIsQ0FBQ2dDLElBQUQsRUFBTWUsb0JBQU4sQ0FBNUI7QUFDQTlDLDJFQUFlO0FBQ2xCLEdBdEIrQyxDQXdCaEQ7OztBQUNBLE1BQUlzQixLQUFLLENBQUNlLE1BQU4sQ0FBYUMsU0FBYixDQUF1QkMsUUFBdkIsQ0FBZ0MsdUJBQWhDLENBQUosRUFBOEQ7QUFDMUQsUUFBSU8scUJBQW9CLEdBQUd4QixLQUFLLENBQUNlLE1BQU4sQ0FBYXRCLGFBQXhDO0FBQUEsUUFDSVosUUFBTyxHQUFHbUIsS0FBSyxDQUFDZSxNQUFOLENBQWFHLE9BQWIsQ0FBcUJyQyxPQURuQztBQUFBLFFBRUk0QixLQUFHLEdBQUcsNEJBQTRCNUIsUUFGdEM7O0FBSUFKLDBIQUE0QixDQUFDZ0MsS0FBRCxFQUFNZSxxQkFBTixDQUE1QjtBQUNBOUMsMkVBQWU7QUFDbEIsR0FoQytDLENBa0NoRDs7O0FBQ0EsTUFBSXNCLEtBQUssQ0FBQ2UsTUFBTixDQUFhQyxTQUFiLENBQXVCQyxRQUF2QixDQUFnQyxzQkFBaEMsQ0FBSixFQUE2RDtBQUN6RCxRQUFJTyxzQkFBb0IsR0FBR3hCLEtBQUssQ0FBQ2UsTUFBTixDQUFhdEIsYUFBeEM7QUFBQSxRQUNJZ0MsY0FBYyxHQUFHekIsS0FBSyxDQUFDZSxNQUFOLENBQWFHLE9BQWIsQ0FBcUJPLGNBRDFDO0FBQUEsUUFFSUgsY0FBYSxHQUFHLDZCQUE2QkcsY0FBN0IsR0FBOEMsSUFGbEU7QUFBQSxRQUdJZCxlQUFjLEdBQUd0QyxRQUFRLENBQUNrRCxnQkFBVCxDQUEwQkQsY0FBMUIsQ0FIckI7QUFBQSxRQUlJYixLQUFHLEdBQUcsMkJBQTJCZ0IsY0FKckM7O0FBTUFoRCwwSEFBNEIsQ0FBQ2dDLEtBQUQsRUFBTWUsc0JBQU4sRUFBNEJiLGVBQTVCLENBQTVCO0FBQ0FqQywyRUFBZTtBQUNsQjtBQUNKLENBN0NELEU7Ozs7Ozs7Ozs7O0FDSEEsSUFBSWdELFlBQVksR0FBR3JELFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixjQUF4QixDQUFuQjtBQUFBLElBQ0lxRCxTQUFTLEdBQUd0RCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsWUFBeEIsQ0FEaEI7QUFBQSxJQUVJc0QsaUJBQWlCLEdBQUd2RCxRQUFRLENBQUNDLGNBQVQsQ0FBd0IsbUJBQXhCLENBRnhCOztBQUlBLElBQUlvRCxZQUFKLEVBQWtCO0FBQ2RBLGNBQVksQ0FBQ2xELGdCQUFiLENBQThCLFFBQTlCLEVBQXdDLFVBQVV3QixLQUFWLEVBQWlCO0FBQ3JEQSxTQUFLLENBQUNDLGNBQU47QUFFQSxRQUFJbkIsT0FBSixDQUFZLFVBQVVDLE9BQVYsRUFBbUJDLE1BQW5CLEVBQTJCO0FBQ25DLFVBQUlDLFdBQVcsR0FBRyxJQUFJQyxjQUFKLEVBQWxCO0FBQUEsVUFDSWdCLFFBQVEsR0FBRyxJQUFJQyxRQUFKLENBQWF1QixZQUFiLENBRGY7QUFHQXpDLGlCQUFXLENBQUNULGdCQUFaLENBQTZCLE1BQTdCLEVBQXFDLFlBQVk7QUFDN0MsWUFBSSxLQUFLVyxVQUFMLEtBQW9CLENBQXhCLEVBQTJCO0FBQ3ZCLGNBQUksS0FBS0MsTUFBTCxLQUFnQixHQUFwQixFQUF5QjtBQUNyQkwsbUJBQU8sQ0FBQ3FCLElBQUksQ0FBQ0MsS0FBTCxDQUFXLEtBQUtDLFFBQWhCLENBQUQsQ0FBUDtBQUNILFdBRkQsTUFFTztBQUNIdEIsa0JBQU0sQ0FBQyxLQUFLSSxNQUFOLENBQU47QUFDSDtBQUNKO0FBQ0osT0FSRDtBQVNBSCxpQkFBVyxDQUFDSSxJQUFaLENBQWlCLE1BQWpCLEVBQXlCLFlBQXpCO0FBQ0FKLGlCQUFXLENBQUNLLGdCQUFaLENBQTZCLGtCQUE3QixFQUFpRCxnQkFBakQ7QUFFQUwsaUJBQVcsQ0FBQ00sSUFBWixDQUFpQlcsUUFBakI7QUFDSCxLQWpCRCxFQWtCS1YsSUFsQkwsQ0FrQlUsVUFBQ2UsSUFBRCxFQUFVO0FBQ1o7QUFDQXFCLHVCQUFpQixDQUFDQyxrQkFBbEIsQ0FBcUMsV0FBckMsRUFBa0RDLG1CQUFtQixDQUFDdkIsSUFBRCxDQUFyRTtBQUNBb0IsZUFBUyxDQUFDSSxJQUFWO0FBQ0FMLGtCQUFZLENBQUNNLEtBQWI7QUFDSCxLQXZCTCxFQXdCS3JDLEtBeEJMLENBd0JXLFVBQUNDLEtBQUQsRUFBVztBQUNkQyxhQUFPLENBQUNELEtBQVIsQ0FBY0EsS0FBZDtBQUNILEtBMUJMO0FBMkJILEdBOUJEO0FBK0JILEMsQ0FFRDs7O0FBQ0EsU0FBU2tDLG1CQUFULENBQTZCdkIsSUFBN0IsRUFBbUM7QUFDL0IsU0FBTyw4Q0FBOENBLElBQUksQ0FBQ1ksRUFBbkQsR0FBd0QsSUFBeEQsR0FDSCx3QkFERyxHQUN3QlosSUFBSSxDQUFDMEIsSUFEN0IsR0FDb0MsTUFEcEMsR0FFSCxzQkFGRyxHQUlILGlGQUpHLEdBS0gsdUZBTEcsR0FPSCx1Q0FQRyxHQU91QzFCLElBQUksQ0FBQzJCLElBQUwsQ0FBVUMsUUFQakQsR0FPNEQsU0FQNUQsR0FRSCxRQVJHLEdBU0gsUUFUSjtBQVVILEMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwicmVxdWlyZSgnLi90d2VldC9uZXcuanMnKTtcbnJlcXVpcmUoJy4vdHdlZXQvZWRpdC5qcycpO1xucmVxdWlyZSgnLi90d2VldC9kZWxldGUuanMnKTtcblxucmVxdWlyZSgnLi90d2VldC9tYW5hZ2VBY3Rpb25zLmpzJyk7XG5cbnJlcXVpcmUoJy4vZmF2b3JpdGVzL2xpc3QuanMnKTtcblxuXG5yZXF1aXJlKCcuLy4uL2Nzcy9hcHAuY3NzJyk7XG4iLCJpbXBvcnQge3JlbmRlckRhdGFGcm9tVXJsSW5zaWRlQmxvY2t9IGZyb20gXCIuLy4uL2Z1bmN0aW9ucy9yZW5kZXIvcmVuZGVyRGF0YUZyb21VcmxJbnNpZGVCbG9jay5qc1wiO1xuXG51cGRhdGVGYXZvcml0ZXNCeUNsaWNrKCk7XG5cbmV4cG9ydCBmdW5jdGlvbiB1cGRhdGVGYXZvcml0ZXNCeUNsaWNrKCkge1xuICAgIGxldCBzaG93RmF2b3JpdGVMaXN0QnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Nob3dGYXZvcml0ZUxpc3RCdXR0b24nKSxcbiAgICAgICAgZmF2b3JpdGVMaXN0Qm9keSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmYXZvcml0ZUxpc3RCb2R5Jyk7XG5cbiAgICBpZiAoc2hvd0Zhdm9yaXRlTGlzdEJ1dHRvbikge1xuICAgICAgICBzaG93RmF2b3JpdGVMaXN0QnV0dG9uLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmVuZGVyRGF0YUZyb21VcmxJbnNpZGVCbG9jaygnZmF2b3JpdGVzJywgZmF2b3JpdGVMaXN0Qm9keSlcbiAgICAgICAgfSk7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gdXBkYXRlRmF2b3JpdGVzKCkge1xuICAgIGxldCBzaG93RmF2b3JpdGVMaXN0QnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3Nob3dGYXZvcml0ZUxpc3RCdXR0b24nKSxcbiAgICAgICAgZmF2b3JpdGVMaXN0Qm9keSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdmYXZvcml0ZUxpc3RCb2R5Jyk7XG5cbiAgICByZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrKCdmYXZvcml0ZXMnLCBmYXZvcml0ZUxpc3RCb2R5KVxufVxuIiwiZXhwb3J0IGZ1bmN0aW9uIGhhbmRsZVR3ZWV0RGVsZXRlKHR3ZWV0Q29udGVudFdyYXBwZXIsIHR3ZWV0SWQpIHtcblxuICAgIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgbGV0IGh0dHBSZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG5cbiAgICAgICAgaHR0cFJlcXVlc3QuYWRkRXZlbnRMaXN0ZW5lcignbG9hZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zdGF0dXMgPT09IDIwMCkge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKClcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QodGhpcy5zdGF0dXMpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgaHR0cFJlcXVlc3Qub3BlbihcIkRFTEVURVwiLCAnL3R3ZWV0LycgKyB0d2VldElkICsgJy9kZWxldGUnKTtcbiAgICAgICAgaHR0cFJlcXVlc3Quc2V0UmVxdWVzdEhlYWRlcignWC1SZXF1ZXN0ZWQtV2l0aCcsICdYTUxIdHRwUmVxdWVzdCcpO1xuXG4gICAgICAgIGh0dHBSZXF1ZXN0LnNlbmQoKTtcbiAgICB9KVxuICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgICB0d2VldENvbnRlbnRXcmFwcGVyLnBhcmVudEVsZW1lbnQucmVtb3ZlQ2hpbGQodHdlZXRDb250ZW50V3JhcHBlcik7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpXG4gICAgICAgIH0pXG59XG4iLCJleHBvcnQgZnVuY3Rpb24gaGFuZGxlVHdlZXRVcGRhdGUoZm9ybSwgdHdlZXRDb250ZW50V3JhcHBlciwgdHdlZXRJZCkge1xuICAgIGZvcm0uYWRkRXZlbnRMaXN0ZW5lcignc3VibWl0JywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgICAgIGxldCBodHRwUmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpLFxuICAgICAgICAgICAgICAgIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKGZvcm0pO1xuXG4gICAgICAgICAgICBodHRwUmVxdWVzdC5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoSlNPTi5wYXJzZSh0aGlzLnJlc3BvbnNlKSlcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdCh0aGlzLnN0YXR1cylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaHR0cFJlcXVlc3Qub3BlbihcIlBPU1RcIiwgJy90d2VldC8nICsgdHdlZXRJZCArICcvZWRpdCcpO1xuICAgICAgICAgICAgaHR0cFJlcXVlc3Quc2V0UmVxdWVzdEhlYWRlcignWC1SZXF1ZXN0ZWQtV2l0aCcsICdYTUxIdHRwUmVxdWVzdCcpO1xuXG4gICAgICAgICAgICBodHRwUmVxdWVzdC5zZW5kKGZvcm1EYXRhKTtcbiAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgdHdlZXRDb250ZW50V3JhcHBlci5pbm5lckhUTUwgPSBkYXRhO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKVxuICAgICAgICAgICAgfSlcbiAgICB9KTtcbn1cbiIsImV4cG9ydCBmdW5jdGlvbiByZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrKHVybCwgdHdlZXRXcmFwcGVyLCBidXR0b25Ob2RlTGlzdCA9IG51bGwpIHtcbiAgICBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGxldCBodHRwUmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgICAgIGh0dHBSZXF1ZXN0LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5yZWFkeVN0YXRlID09PSA0KSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShKU09OLnBhcnNlKHRoaXMucmVzcG9uc2UpKVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCh0aGlzLnN0YXR1cylcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBodHRwUmVxdWVzdC5vcGVuKFwiR0VUXCIsIHVybCk7XG4gICAgICAgIGh0dHBSZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoJ1gtUmVxdWVzdGVkLVdpdGgnLCAnWE1MSHR0cFJlcXVlc3QnKTtcbiAgICAgICAgaHR0cFJlcXVlc3Quc2VuZCgpO1xuICAgIH0pXG4gICAgICAgIC50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICBpZiAoYnV0dG9uTm9kZUxpc3QpIHtcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGJ1dHRvbk5vZGVMaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIGJ1dHRvbk5vZGVMaXN0W2ldLnBhcmVudEVsZW1lbnQuaW5uZXJIVE1MID0gZGF0YTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHR3ZWV0V3JhcHBlci5pbm5lckhUTUwgPSBkYXRhO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKVxuICAgICAgICB9KVxufVxuIiwiZXhwb3J0IGZ1bmN0aW9uIHJlbmRlclR3ZWV0RWRpdEZvcm0odHdlZXRJZCwgdHdlZXRXcmFwcGVyKSB7XG4gICAgbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBsZXQgaHR0cFJlcXVlc3QgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICBodHRwUmVxdWVzdC5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHRoaXMucmVhZHlTdGF0ZSA9PT0gNCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXR1cyA9PT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoSlNPTi5wYXJzZSh0aGlzLnJlc3BvbnNlKSlcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZWplY3QodGhpcy5zdGF0dXMpXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgaHR0cFJlcXVlc3Qub3BlbihcIkdFVFwiLCAnL3R3ZWV0LycgKyB0d2VldElkICsgJy9lZGl0Jyk7XG4gICAgICAgIGh0dHBSZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoJ1gtUmVxdWVzdGVkLVdpdGgnLCAnWE1MSHR0cFJlcXVlc3QnKTtcbiAgICAgICAgaHR0cFJlcXVlc3Quc2VuZCgpO1xuICAgIH0pXG4gICAgICAgIC50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICB0d2VldFdyYXBwZXIuaW5uZXJIVE1MID0gZGF0YTtcbiAgICAgICAgfSlcbiAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcilcbiAgICAgICAgfSlcbn1cbiIsIi8vIEZ1bmN0aW9uIHRvIGhhbmRsZSB0d2VldCBkZWxldGVcbmltcG9ydCB7aGFuZGxlVHdlZXREZWxldGV9IGZyb20gXCIuLy4uL2Z1bmN0aW9ucy9oYW5kbGUvdHdlZXREZWxldGUuanNcIjtcblxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgIGlmIChldmVudC50YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkZWxldGUtdHdlZXQtYnV0dG9uJykpIHtcblxuICAgICAgICBsZXQgdHdlZXRXcmFwcGVyID0gZXZlbnQudGFyZ2V0LnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudCxcbiAgICAgICAgICAgIHR3ZWV0SWQgPSB0d2VldFdyYXBwZXIuZGF0YXNldC50d2VldElkO1xuICAgICAgICBoYW5kbGVUd2VldERlbGV0ZSh0d2VldFdyYXBwZXIsIHR3ZWV0SWQpO1xuICAgIH1cbn0pO1xuIiwiLy8gRnVuY3Rpb24gdG8gaGFuZGxlIHR3ZWV0IHVwZGF0ZVxuaW1wb3J0IHtoYW5kbGVUd2VldFVwZGF0ZX0gZnJvbSBcIi4vLi4vZnVuY3Rpb25zL2hhbmRsZS90d2VldFVwZGF0ZS5qc1wiO1xuLy8gQWRkaXRpb25hbCBmdW5jdGlvbnMgdG8gcmVuZGVyIHRlbXBsYXRlc1xuaW1wb3J0IHtyZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrfSBmcm9tIFwiLi8uLi9mdW5jdGlvbnMvcmVuZGVyL3JlbmRlckRhdGFGcm9tVXJsSW5zaWRlQmxvY2suanNcIjtcbmltcG9ydCB7cmVuZGVyVHdlZXRFZGl0Rm9ybX0gZnJvbSBcIi4vLi4vZnVuY3Rpb25zL3JlbmRlci90d2VldEVkaXRGb3JtLmpzXCI7XG5cbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XG5cbiAgICBpZiAoZXZlbnQudGFyZ2V0LmNsYXNzTGlzdC5jb250YWlucygnZWRpdC10d2VldC1idXR0b24nKSkge1xuXG4gICAgICAgIGxldCB0d2VldFdyYXBwZXIgPSBldmVudC50YXJnZXQucGFyZW50RWxlbWVudC5wYXJlbnRFbGVtZW50LFxuICAgICAgICAgICAgdHdlZXRJZCA9IHR3ZWV0V3JhcHBlci5kYXRhc2V0LnR3ZWV0SWQ7XG5cbiAgICAgICAgcmVuZGVyVHdlZXRFZGl0Rm9ybSh0d2VldElkLCB0d2VldFdyYXBwZXIpO1xuXG4gICAgfSBlbHNlIGlmIChldmVudC50YXJnZXQuaWQgPT0gJ2Nsb3NlX2VkaXRpbmdfdHdlZXQnKSB7XG5cbiAgICAgICAgbGV0IHR3ZWV0V3JhcHBlciA9IGV2ZW50LnRhcmdldC5wYXJlbnRFbGVtZW50LnBhcmVudEVsZW1lbnQsXG4gICAgICAgICAgICB0d2VldElkID0gdHdlZXRXcmFwcGVyLmRhdGFzZXQudHdlZXRJZCxcbiAgICAgICAgICAgIHVybCA9ICcvdHdlZXQvJyArIHR3ZWV0SWQgKyAnL3Nob3cnO1xuXG4gICAgICAgIHJlbmRlckRhdGFGcm9tVXJsSW5zaWRlQmxvY2sodXJsLCB0d2VldFdyYXBwZXIpXG5cbiAgICB9IGVsc2UgaWYgKGV2ZW50LnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2VkaXRlZF90d2VldF9zYXZlJykpIHtcblxuICAgICAgICBsZXQgdHdlZXRXcmFwcGVyID0gZXZlbnQudGFyZ2V0LnBhcmVudEVsZW1lbnQucGFyZW50RWxlbWVudC5wYXJlbnRFbGVtZW50LFxuICAgICAgICAgICAgZm9ybSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdlZGl0VHdlZXRGb3JtJyksXG4gICAgICAgICAgICB0d2VldElkID0gdHdlZXRXcmFwcGVyLmRhdGFzZXQudHdlZXRJZDtcblxuICAgICAgICBoYW5kbGVUd2VldFVwZGF0ZShmb3JtLCB0d2VldFdyYXBwZXIsIHR3ZWV0SWQpO1xuICAgIH1cbn0pO1xuIiwiaW1wb3J0IHtyZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrfSBmcm9tIFwiLi8uLi9mdW5jdGlvbnMvcmVuZGVyL3JlbmRlckRhdGFGcm9tVXJsSW5zaWRlQmxvY2suanNcIjtcbmltcG9ydCB7dXBkYXRlRmF2b3JpdGVzfSBmcm9tIFwiLi8uLi9mYXZvcml0ZXMvbGlzdFwiO1xuXG5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgLy8gTWFuYWdlIHVzZXIgc3Vic2NyaXB0aW9uc1xuICAgIGlmIChldmVudC50YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdmb2xsb3ctdXNlci1idXR0b24nKSkge1xuICAgICAgICBsZXQgZm9sbG93VXNlcldyYXBwZXIgPSBldmVudC50YXJnZXQucGFyZW50RWxlbWVudCxcbiAgICAgICAgICAgIHVzZXJJZCA9IGV2ZW50LnRhcmdldC5kYXRhc2V0LnVzZXJJZCxcbiAgICAgICAgICAgIGRhdGFBdHRyaWJ1dGUgPSAnW2RhdGEtdXNlci1pZD1cIicgKyB1c2VySWQgKyAnXCJdJyxcbiAgICAgICAgICAgIGJ1dHRvbk5vZGVMaXN0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChkYXRhQXR0cmlidXRlKSxcbiAgICAgICAgICAgIHVybCA9ICcvZm9sbG93L3VzZXIvJyArIHVzZXJJZDtcblxuICAgICAgICByZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrKHVybCwgZm9sbG93VXNlcldyYXBwZXIsIGJ1dHRvbk5vZGVMaXN0KTtcbiAgICAgICAgdXBkYXRlRmF2b3JpdGVzKCk7XG4gICAgfVxuXG4gICAgLy8gTWFuYWdlIHR3ZWV0IGxpa2VzXG4gICAgaWYgKGV2ZW50LnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2xpa2UtdHdlZXQtYnV0dG9uJykpIHtcbiAgICAgICAgbGV0IGZhdm9yaXRlVHdlZXRXcmFwcGVyID0gZXZlbnQudGFyZ2V0LnBhcmVudEVsZW1lbnQsXG4gICAgICAgICAgICB0d2VldElkID0gZXZlbnQudGFyZ2V0LmRhdGFzZXQudHdlZXRJZCxcbiAgICAgICAgICAgIHVybCA9ICcvbWFuYWdlL2xpa2UvdHdlZXQvJyArIHR3ZWV0SWQ7XG5cbiAgICAgICAgcmVuZGVyRGF0YUZyb21VcmxJbnNpZGVCbG9jayh1cmwsIGZhdm9yaXRlVHdlZXRXcmFwcGVyKTtcbiAgICAgICAgdXBkYXRlRmF2b3JpdGVzKCk7XG4gICAgfVxuXG4gICAgLy8gTWFuYWdlIGZhdm9yaXRlIHR3ZWV0c1xuICAgIGlmIChldmVudC50YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdmYXZvcml0ZS10d2VldC1idXR0b24nKSkge1xuICAgICAgICBsZXQgZmF2b3JpdGVUd2VldFdyYXBwZXIgPSBldmVudC50YXJnZXQucGFyZW50RWxlbWVudCxcbiAgICAgICAgICAgIHR3ZWV0SWQgPSBldmVudC50YXJnZXQuZGF0YXNldC50d2VldElkLFxuICAgICAgICAgICAgdXJsID0gJy9tYW5hZ2UvZmF2b3JpdGUvdHdlZXQvJyArIHR3ZWV0SWQ7XG5cbiAgICAgICAgcmVuZGVyRGF0YUZyb21VcmxJbnNpZGVCbG9jayh1cmwsIGZhdm9yaXRlVHdlZXRXcmFwcGVyKTtcbiAgICAgICAgdXBkYXRlRmF2b3JpdGVzKCk7XG4gICAgfVxuXG4gICAgLy8gTWFuYWdlIGZhdm9yaXRlIHVzZXJzXG4gICAgaWYgKGV2ZW50LnRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2Zhdm9yaXRlLXVzZXItYnV0dG9uJykpIHtcbiAgICAgICAgbGV0IGZhdm9yaXRlVHdlZXRXcmFwcGVyID0gZXZlbnQudGFyZ2V0LnBhcmVudEVsZW1lbnQsXG4gICAgICAgICAgICBmYXZvcml0ZVVzZXJJZCA9IGV2ZW50LnRhcmdldC5kYXRhc2V0LmZhdm9yaXRlVXNlcklkLFxuICAgICAgICAgICAgZGF0YUF0dHJpYnV0ZSA9ICdbZGF0YS1mYXZvcml0ZS11c2VyLWlkPVwiJyArIGZhdm9yaXRlVXNlcklkICsgJ1wiXScsXG4gICAgICAgICAgICBidXR0b25Ob2RlTGlzdCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZGF0YUF0dHJpYnV0ZSksXG4gICAgICAgICAgICB1cmwgPSAnL21hbmFnZS9mYXZvcml0ZS91c2VyLycgKyBmYXZvcml0ZVVzZXJJZDtcblxuICAgICAgICByZW5kZXJEYXRhRnJvbVVybEluc2lkZUJsb2NrKHVybCwgZmF2b3JpdGVUd2VldFdyYXBwZXIsIGJ1dHRvbk5vZGVMaXN0KTtcbiAgICAgICAgdXBkYXRlRmF2b3JpdGVzKCk7XG4gICAgfVxufSk7XG4iLCJsZXQgbmV3VHdlZXRGb3JtID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ25ld1R3ZWV0Rm9ybScpLFxuICAgIHR3ZWV0U2F2ZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCd0d2VldF9zYXZlJyksXG4gICAgbXlUd2VldHNDb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXktdHdlZXRzLXdyYXBwZXInKTtcblxuaWYgKG5ld1R3ZWV0Rm9ybSkge1xuICAgIG5ld1R3ZWV0Rm9ybS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICBsZXQgaHR0cFJlcXVlc3QgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKSxcbiAgICAgICAgICAgICAgICBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YShuZXdUd2VldEZvcm0pO1xuXG4gICAgICAgICAgICBodHRwUmVxdWVzdC5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RhdHVzID09PSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoSlNPTi5wYXJzZSh0aGlzLnJlc3BvbnNlKSlcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdCh0aGlzLnN0YXR1cylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgaHR0cFJlcXVlc3Qub3BlbihcIlBPU1RcIiwgJy90d2VldC9uZXcnKTtcbiAgICAgICAgICAgIGh0dHBSZXF1ZXN0LnNldFJlcXVlc3RIZWFkZXIoJ1gtUmVxdWVzdGVkLVdpdGgnLCAnWE1MSHR0cFJlcXVlc3QnKTtcblxuICAgICAgICAgICAgaHR0cFJlcXVlc3Quc2VuZChmb3JtRGF0YSlcbiAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XG4gICAgICAgICAgICAgICAgLy9pbnNlcnQgbmV3IFR3ZWV0IGFzIGEgbGFzdCBlbGVtZW50IG9mIG15IHR3ZWV0cyBsaXN0XG4gICAgICAgICAgICAgICAgbXlUd2VldHNDb250YWluZXIuaW5zZXJ0QWRqYWNlbnRIVE1MKCdiZWZvcmVlbmQnLCBnZXROZXdUd2VldFRlbXBsYXRlKGRhdGEpKTtcbiAgICAgICAgICAgICAgICB0d2VldFNhdmUuYmx1cigpO1xuICAgICAgICAgICAgICAgIG5ld1R3ZWV0Rm9ybS5yZXNldCgpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKVxuICAgICAgICAgICAgfSlcbiAgICB9KTtcbn1cblxuLy8gZ2V0IGh0bWwgZm9yIG5ldyB0d2VldCBibG9ja1xuZnVuY3Rpb24gZ2V0TmV3VHdlZXRUZW1wbGF0ZShkYXRhKSB7XG4gICAgcmV0dXJuIFwiPGRpdiBjbGFzcz0nY2FyZCBtLTMgcC0xJyBkYXRhLXR3ZWV0LWlkPSdcIiArIGRhdGEuaWQgKyBcIic+XCIgK1xuICAgICAgICBcIjxwIGNsYXNzPSd0d2VldC1ib2R5Jz5cIiArIGRhdGEubmFtZSArIFwiPC9wPlwiICtcbiAgICAgICAgXCI8ZGl2IGNsYXNzPSdkLWZsZXgnPlwiICtcblxuICAgICAgICBcIjxidXR0b24gY2xhc3M9J2VkaXQtdHdlZXQtYnV0dG9uIGJ0biBidG4tc20gYnRuLW91dGxpbmUtaW5mbyBtLTEnPkVkaXQ8L2J1dHRvbj5cIiArXG4gICAgICAgIFwiPGJ1dHRvbiBjbGFzcz0nZGVsZXRlLXR3ZWV0LWJ1dHRvbiBidG4gYnRuLXNtIGJ0bi1vdXRsaW5lLWRhbmdlciBtLTEnPkRlbGV0ZTwvYnV0dG9uPlwiICtcblxuICAgICAgICBcIjxzcGFuIGNsYXNzPSdtdC0xIG1iLTEgbWwtYXV0byBtci0xJz5cIiArIGRhdGEudXNlci51c2VybmFtZSArIFwiPC9zcGFuPlwiICtcbiAgICAgICAgXCI8L2Rpdj5cIiArXG4gICAgICAgIFwiPC9kaXY+XCJcbn0iXSwic291cmNlUm9vdCI6IiJ9