<?php

namespace App\Controller;

use App\Entity\Media;
use App\Entity\User;
use App\Form\ImageType;
use App\Util\ImageUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/show", name="show_profile")
     */
    public function show()
    {
        return $this->render('profile/show.html.twig', [

        ]);
    }

    /**
     * @Route("/upload/image", name="upload_image")
     * @param Request $request
     * @param ImageUploader $imageUploader
     * @return Response
     * @throws \Exception
     */
    public function addImage(Request $request, ImageUploader $imageUploader): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();
        $media = new Media();

        $form = $this->createForm(ImageType::class, $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $image */
            $image = $form->get('image')->getData();
            $imageName = $imageUploader->upload($image);

            $media->setImage($imageName);
            $user->setMedia($media);

            $entityManager->persist($media);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('profile/image_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
