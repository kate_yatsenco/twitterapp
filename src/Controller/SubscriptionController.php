<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends AbstractController
{
    /**
     * @Route("/follow/user/{id}", name="follow_user")
     * @param Request $request
     * @param User $userToSubscribe
     * @return Response
     */
    public function follow(Request $request, User $userToSubscribe)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if ($request->isXMLHttpRequest()) {
            if ($currentUser->getFollowing()->contains($userToSubscribe)) {
                $currentUser->unFollow($currentUser, $userToSubscribe);
            } else {
                $currentUser->follow($currentUser, $userToSubscribe);
            }
            $entityManager->flush();

            return new JsonResponse($this->render('buttons/follow_user.html.twig', [
                'user' => $userToSubscribe,
            ])->getContent());
        }

        return new Response('This is only accessible in AJAX!', 500);
    }
}
