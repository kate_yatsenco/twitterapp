<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FavoriteController extends AbstractController
{
    /**
     * @Route("/manage/favorite/tweet/{id}", name="manage_favorite_tweet")
     * @param Request $request
     * @param Tweet $tweet
     * @return JsonResponse|Response
     */
    public function addToFavoriteTweet(Request $request, Tweet $tweet)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXMLHttpRequest()) {
            if ($user->getFavoriteTweets()->contains($tweet)) {
                $user->removeTweetFromFavorite($tweet);
            } else {
                $user->addFavoriteTweet($tweet);
            }

            $entityManager->flush();

            return new JsonResponse($this->render('buttons/favorite_tweet.html.twig', [
                'tweet' => $tweet,
            ])->getContent());
        }
        return new Response('This is only accessible in AJAX!', 500);
    }

    /**
     * @Route("/manage/favorite/user/{id}", name="manage_favorite_user")
     * @param Request $request
     * @param User $favoriteUser
     * @return Response
     */
    public function addToFavoriteUser(Request $request, User $favoriteUser)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXMLHttpRequest()) {
            if ($user->getFavoriteUsers()->contains($favoriteUser)) {
                $user->removeFavoriteUser($favoriteUser);
            } else {
                $user->addFavoriteUser($favoriteUser);
            }

            $entityManager->flush();

            return new JsonResponse($this->render('buttons/favorite_user.html.twig', [
                'user' => $favoriteUser,
            ])->getContent());
        }
        return new Response('This is only accessible in AJAX!', 500);
    }
}
