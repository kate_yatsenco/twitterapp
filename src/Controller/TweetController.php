<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Form\TweetType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/tweet")
 */
class TweetController extends AbstractController
{
    /**
     * @Route("/new", name="tweet_new", methods="GET|POST")
     * @param Request $request
     * @return JsonResponse|Response
     *
     */
    public function new(Request $request)
    {
        $tweet = new Tweet();
        $form = $this->createForm(TweetType::class, $tweet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($request->isXMLHttpRequest()) {
                $tweet->setUser($this->getUser());
                $em->persist($tweet);
                $em->flush();

                $encoders = array(new JsonEncoder());
                $normalizers = array(new ObjectNormalizer());

                $serializer = new Serializer($normalizers, $encoders);

                $data = $serializer->serialize($tweet, 'json', ['attributes' => ['id', 'name', 'user' => ['username']]]);
                return new JsonResponse($data, 200, [], true);

            } else {
                return new Response('This is only accessible in AJAX!', 500);
            }
        }

        return $this->render('tweet/new.html.twig', [
            'tweet' => $tweet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tweet_edit", methods="GET|POST")
     * @param Request $request
     * @param Tweet $tweet
     * @return JsonResponse|Response
     */
    public function edit(Request $request, Tweet $tweet)
    {
        $form = $this->createForm(TweetType::class, $tweet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($request->isXMLHttpRequest()) {
                $em->flush();

                return new JsonResponse($this->render('includes/my_tweet.html.twig', [
                    'tweet' => $tweet,
                ])->getContent());

            } else {
                return new Response('This is only accessible in AJAX!', 500);
            }
        }

        return new JsonResponse($this->render('tweet/edit.html.twig', [
            'tweet' => $tweet,
            'form' => $form->createView(),
        ])->getContent());
    }

    /**
     * @Route("/{id}/show", name="tweet_show", methods="GET|POST")
     * @param Request $request
     * @param Tweet $tweet
     * @return JsonResponse|Response
     */
    public function show(Request $request, Tweet $tweet)
    {
        if ($request->isXMLHttpRequest()) {
            return new JsonResponse($this->render('includes/my_tweet.html.twig', [
                'tweet' => $tweet,
            ])->getContent());
        }
        return new Response('This is only accessible in AJAX!', 500);
    }

    /**
     * @Route("/{id}/delete", name="tweet_delete", methods="DELETE")
     * @param Request $request
     * @param Tweet $tweet
     * @return Response
     */
    public function delete(Request $request, Tweet $tweet): Response
    {
        if ($request->isXMLHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tweet);
            $em->flush();
            return new Response("Tweet deleted!", 200);
        }
        return new Response('This is only accessible in AJAX!', 500);
    }
}
