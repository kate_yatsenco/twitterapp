<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @return Response
     */
    public function index()
    {
        $user = $this->getUser();

        $tweetRepository = $this->getDoctrine()->getRepository(Tweet::class);

        $myTweets = $tweetRepository->findAllTweetsOwnedByUser($user);
        $otherUsersTweets = $tweetRepository->findAllTweetsNotOwnedByUser($user);

        return $this->render('main/index.html.twig', [
            'my_tweets' => $myTweets,
            'other_users_tweets' => $otherUsersTweets
        ]);
    }

    /**
     * @Route("/favorites", name="favorites")
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function showFavorites(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $favoriteTweetsArray = $user->getFavoriteTweets()->toArray();
        $favoriteUsersArray = $user->getFavoriteUsers()->toArray();

        if ($request->isXMLHttpRequest()) {
            return new JsonResponse($this->render('favorites/show.html.twig', [
                'favorite_tweets' => $favoriteTweetsArray,
                'favorite_users' => $favoriteUsersArray
            ])->getContent());
        }
        return new Response('This is only accessible in AJAX!', 500);
    }
}
