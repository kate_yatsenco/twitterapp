<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LikeController extends AbstractController
{
    /**
     * @Route("/manage/like/tweet/{id}", name="manage_like_tweet")
     * @param Request $request
     * @param Tweet $tweet
     * @return JsonResponse|Response
     */
    public function likeTweet(Request $request, Tweet $tweet)
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $this->getUser();

        if ($request->isXMLHttpRequest()) {
            if ($user->getLikedTweets()->contains($tweet)) {
                $user->removeLikeFromTweet($tweet);
            } else {
                $user->addLikeToTweet($tweet);
            }

            $entityManager->flush();

            return new JsonResponse($this->render('buttons/like_tweet.html.twig', [
                'tweet' => $tweet,
            ])->getContent());
        }
        return new Response('This is only accessible in AJAX!', 500);
    }
}
