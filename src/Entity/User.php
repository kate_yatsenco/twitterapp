<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * Many Users have Many favorite Tweets.
     * @ORM\ManyToMany(targetEntity="Tweet")
     * @ORM\JoinTable(name="users_favorite_tweets",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="favorite_tweet_id", referencedColumnName="id")}
     *      )
     */
    private $favoriteTweets;

    /**
     * Many Users have Many liked Tweets.
     * @ORM\ManyToMany(targetEntity="Tweet")
     * @ORM\JoinTable(name="users_liked_tweets",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="liked_tweet_id", referencedColumnName="id")}
     *      )
     */
    private $likedTweets;

    /**
     * One product has many features. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Tweet", mappedBy="user")
     */

    private $myTweets;

    /**
     * Many Users have many Users.
     * @var User[]| Collection $following
     * @ORM\ManyToMany(targetEntity="User", inversedBy="followers")
     * @ORM\JoinTable(name="subscriptions",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="follow_to_user_id", referencedColumnName="id")}
     *      )
     */
    private $following;

    /**
     * Many Users have Many Users.
     * @var User[]| Collection $followers
     * @ORM\ManyToMany(targetEntity="User", mappedBy="following")
     */
    private $followers;

    /**
     * @ORM\Column(name="roles", type="json", nullable=true)
     */
    private $roles = [];

    /**
     * @Assert\NotBlank
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @var string The hashed password
     * @ORM\Column(name="password", type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="favoriteUsers")
     */
    private $favoriteUsers;

    /**
     * One User has One Media.
     * @ORM\OneToOne(targetEntity="Media")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;

    public function __construct()
    {
        $this->favoriteTweets = new ArrayCollection();
        $this->likedTweets = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->favoriteUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Tweet[]
     */
    public function getFavoriteTweets(): Collection
    {
        return $this->favoriteTweets;
    }

    /**
     * @param Tweet $favoriteTweet
     * @return User
     */
    public function addFavoriteTweet(Tweet $favoriteTweet): self
    {
        if (!$this->favoriteTweets->contains($favoriteTweet)) {
            $this->favoriteTweets[] = $favoriteTweet;
        }

        return $this;
    }

    /**
     * @param Tweet $favoriteTweet
     * @return User
     */
    public function removeTweetFromFavorite(Tweet $favoriteTweet): self
    {
        if ($this->favoriteTweets->contains($favoriteTweet)) {
            $this->favoriteTweets->removeElement($favoriteTweet);
        }

        return $this;
    }

    /**
     * @return Collection|Tweet[]
     */
    public function getLikedTweets(): Collection
    {
        return $this->likedTweets;
    }

    public function addLikeToTweet(Tweet $tweet): self
    {
        if (!$this->likedTweets->contains($tweet)) {
            $this->likedTweets[] = $tweet;
        }

        return $this;
    }

    public function removeLikeFromTweet(Tweet $tweet): self
    {
        if ($this->likedTweets->contains($tweet)) {
            $this->likedTweets->removeElement($tweet);
        }

        return $this;
    }

    /**
     * @return Tweet[]
     */
    public function getMyTweets()
    {
        return $this->myTweets;
    }

    /**
     * @param Tweet $myTweets
     */
    public function setMyTweets($myTweets)
    {
        $this->myTweets = $myTweets;
    }

    /**
     * A visual identifier that represents this user.
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @param User $currentUser
     * @param User $subscribeToUser
     * @return User
     * @throws \Exception
     */
    public function follow(User $currentUser, User $subscribeToUser): User
    {
        if (!$this->following->contains($subscribeToUser)) {
            try {
                $this->following->add($subscribeToUser);
            } catch (\Exception $exception) {
                throw new \Exception("Can't follow " . $subscribeToUser->getUsername() . "user");
            }
        }

        if (!$subscribeToUser->followers->contains($currentUser)) {
            try {
                $subscribeToUser->followers->add($currentUser);
            } catch (\Exception $exception) {
                throw new \Exception("Can't add you to subscribed" . $subscribeToUser->getUsername() . "user");
            }
        }

        return $this;
    }

    /**
     * @param User $currentUser
     * @param User $interestingUser
     * @return User
     */
    public function unFollow(User $currentUser, User $interestingUser): User
    {
        if ($interestingUser->followers->contains($currentUser)) {
            $interestingUser->followers->removeElement($currentUser);
        }
        if ($this->following->contains($interestingUser)) {
            $this->following->removeElement($interestingUser);
        }
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getFollowers(): ?Collection
    {
        return $this->followers;
    }

    /**
     * @return User[]|Collection|null
     */
    public function getFollowing(): ?Collection
    {
        return $this->following;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = [];
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|self[]
     */
    public function getFavoriteUsers(): Collection
    {
        return $this->favoriteUsers;
    }

    public function addFavoriteUser(self $favoriteUser): self
    {
        if (!$this->favoriteUsers->contains($favoriteUser)) {
            $this->favoriteUsers[] = $favoriteUser;
        }

        return $this;
    }

    public function removeFavoriteUser(self $favoriteUser): self
    {
        if ($this->favoriteUsers->contains($favoriteUser)) {
            $this->favoriteUsers->removeElement($favoriteUser);
        }

        return $this;
    }

    /**
     * @return Media|null
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param Media $media
     */
    public function setMedia(Media $media)
    {
        $this->media = $media;
    }
}
