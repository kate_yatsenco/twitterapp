<?php

namespace App\Util;

use App\Entity\User;

class SubscriptionManager
{
    public function isSubscribedTo(User $subscriber, User $subscriptionObject): bool
    {
        return $subscriptionObject->getFollowers()->contains($subscriber) ? true : false;
    }
}
