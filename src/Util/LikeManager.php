<?php

namespace App\Util;

use App\Entity\Tweet;
use App\Entity\User;

class LikeManager
{
    public function isLikedTweet(User $user, Tweet $tweet): bool
    {
        return $user->getLikedTweets()->contains($tweet) ? true : false;
    }
}
