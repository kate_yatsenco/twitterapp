<?php

namespace App\Util;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $image)
    {
        $imageName = md5(uniqid()) . '.' . $image->guessExtension();

        try {
            $image->move($this->getTargetDirectory(), $imageName);
        } catch (FileException $e) {
            throw new \Exception("Cannot upload image. " . $e);
        }

        return $imageName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
