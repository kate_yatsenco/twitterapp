<?php

namespace App\Util;

use App\Entity\Tweet;
use App\Entity\User;

class FavoriteManager
{
    public function isFavoriteTweet(User $user, Tweet $tweet): bool
    {
        return $user->getFavoriteTweets()->contains($tweet) ? true : false;
    }

    public function isFavoriteUser(User $currentUser, User $user): bool
    {
        return $currentUser->getFavoriteUsers()->contains($user) ? true : false;
    }
}
