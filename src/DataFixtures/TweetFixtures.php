<?php

namespace App\DataFixtures;

use App\Entity\Tweet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class TweetFixtures extends Fixture
{
    private $faker;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * {@inheritDoc}
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->faker = Factory::create();
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 3; $i++) {
            $user = new User();
            $user->setUsername(strtolower($this->faker->firstName()));
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'test'
            ));

            for ($i = 0; $i < rand(1, 5); $i++) {
                $tweet = new Tweet();
                $tweet->setName($this->getRandomText());
                $tweet->setUser($user);
                $manager->persist($tweet);
                $manager->flush();
            }
            $manager->persist($user);
            $manager->flush();
        }
    }

    /**
     * @param int $mixNbChars
     * @param int $maxNbChars
     * @return mixed
     */
    private function getRandomText(int $mixNbChars = 11, int $maxNbChars = 100)
    {
        return $this->faker->realText($this->faker->numberBetween($mixNbChars, $maxNbChars));
    }
}
