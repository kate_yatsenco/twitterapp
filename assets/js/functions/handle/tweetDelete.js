export function handleTweetDelete(tweetContentWrapper, tweetId) {

    new Promise(function (resolve, reject) {
        let httpRequest = new XMLHttpRequest();

        httpRequest.addEventListener('load', function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve()
                } else {
                    reject(this.status)
                }
            }
        });
        httpRequest.open("DELETE", '/tweet/' + tweetId + '/delete');
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        httpRequest.send();
    })
        .then(() => {
            tweetContentWrapper.parentElement.removeChild(tweetContentWrapper);
        })
        .catch((error) => {
            console.error(error)
        })
}
