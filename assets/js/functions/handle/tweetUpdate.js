export function handleTweetUpdate(form, tweetContentWrapper, tweetId) {
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        new Promise(function (resolve, reject) {
            let httpRequest = new XMLHttpRequest(),
                formData = new FormData(form);

            httpRequest.addEventListener('load', function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        resolve(JSON.parse(this.response))
                    } else {
                        reject(this.status)
                    }
                }
            });
            httpRequest.open("POST", '/tweet/' + tweetId + '/edit');
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            httpRequest.send(formData);
        })
            .then((data) => {
                tweetContentWrapper.innerHTML = data;
            })
            .catch((error) => {
                console.error(error)
            })
    });
}
