export function renderDataFromUrlInsideBlock(url, tweetWrapper, buttonNodeList = null) {
    new Promise(function (resolve, reject) {
        let httpRequest = new XMLHttpRequest();

        httpRequest.addEventListener('load', function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve(JSON.parse(this.response))
                } else {
                    reject(this.status)
                }
            }
        });
        httpRequest.open("GET", url);
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        httpRequest.send();
    })
        .then((data) => {
            if (buttonNodeList) {
                for (let i = 0; i < buttonNodeList.length; i++) {
                    buttonNodeList[i].parentElement.innerHTML = data;
                }
            } else {
                tweetWrapper.innerHTML = data;
            }
        })
        .catch((error) => {
            console.error(error)
        })
}
