export function renderTweetEditForm(tweetId, tweetWrapper) {
    new Promise(function (resolve, reject) {
        let httpRequest = new XMLHttpRequest();

        httpRequest.addEventListener('load', function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve(JSON.parse(this.response))
                } else {
                    reject(this.status)
                }
            }
        });
        httpRequest.open("GET", '/tweet/' + tweetId + '/edit');
        httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        httpRequest.send();
    })
        .then((data) => {
            tweetWrapper.innerHTML = data;
        })
        .catch((error) => {
            console.error(error)
        })
}
