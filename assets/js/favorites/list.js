import {renderDataFromUrlInsideBlock} from "./../functions/render/renderDataFromUrlInsideBlock.js";

updateFavoritesByClick();

export function updateFavoritesByClick() {
    let showFavoriteListButton = document.getElementById('showFavoriteListButton'),
        favoriteListBody = document.getElementById('favoriteListBody');

    if (showFavoriteListButton) {
        showFavoriteListButton.addEventListener('click', function () {
            renderDataFromUrlInsideBlock('favorites', favoriteListBody)
        });
    }
}

export function updateFavorites() {
    let showFavoriteListButton = document.getElementById('showFavoriteListButton'),
        favoriteListBody = document.getElementById('favoriteListBody');

    renderDataFromUrlInsideBlock('favorites', favoriteListBody)
}
