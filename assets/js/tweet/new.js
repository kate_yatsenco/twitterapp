let newTweetForm = document.getElementById('newTweetForm'),
    tweetSave = document.getElementById('tweet_save'),
    myTweetsContainer = document.getElementById('my-tweets-wrapper');

if (newTweetForm) {
    newTweetForm.addEventListener('submit', function (event) {
        event.preventDefault();

        new Promise(function (resolve, reject) {
            let httpRequest = new XMLHttpRequest(),
                formData = new FormData(newTweetForm);

            httpRequest.addEventListener('load', function () {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        resolve(JSON.parse(this.response))
                    } else {
                        reject(this.status)
                    }
                }
            });
            httpRequest.open("POST", '/tweet/new');
            httpRequest.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            httpRequest.send(formData)
        })
            .then((data) => {
                //insert new Tweet as a last element of my tweets list
                myTweetsContainer.insertAdjacentHTML('beforeend', getNewTweetTemplate(data));
                tweetSave.blur();
                newTweetForm.reset();
            })
            .catch((error) => {
                console.error(error)
            })
    });
}

// get html for new tweet block
function getNewTweetTemplate(data) {
    return "<div class='card m-3 p-1' data-tweet-id='" + data.id + "'>" +
        "<p class='tweet-body'>" + data.name + "</p>" +
        "<div class='d-flex'>" +

        "<button class='edit-tweet-button btn btn-sm btn-outline-info m-1'>Edit</button>" +
        "<button class='delete-tweet-button btn btn-sm btn-outline-danger m-1'>Delete</button>" +

        "<span class='mt-1 mb-1 ml-auto mr-1'>" + data.user.username + "</span>" +
        "</div>" +
        "</div>"
}