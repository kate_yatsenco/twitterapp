// Function to handle tweet delete
import {handleTweetDelete} from "./../functions/handle/tweetDelete.js";

document.addEventListener('click', function (event) {

    if (event.target.classList.contains('delete-tweet-button')) {

        let tweetWrapper = event.target.parentElement.parentElement,
            tweetId = tweetWrapper.dataset.tweetId;

        handleTweetDelete(tweetWrapper, tweetId);
    }
});
