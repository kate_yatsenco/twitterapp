// Function to handle tweet update
import {handleTweetUpdate} from "./../functions/handle/tweetUpdate.js";
// Additional functions to render templates
import {renderDataFromUrlInsideBlock} from "./../functions/render/renderDataFromUrlInsideBlock.js";
import {renderTweetEditForm} from "./../functions/render/tweetEditForm.js";

document.addEventListener('click', function (event) {

    if (event.target.classList.contains('edit-tweet-button')) {

        let tweetWrapper = event.target.parentElement.parentElement,
            tweetId = tweetWrapper.dataset.tweetId;

        renderTweetEditForm(tweetId, tweetWrapper);

    } else if (event.target.id == 'close_editing_tweet') {

        let tweetWrapper = event.target.parentElement.parentElement,
            tweetId = tweetWrapper.dataset.tweetId,
            url = '/tweet/' + tweetId + '/show';

        renderDataFromUrlInsideBlock(url, tweetWrapper)

    } else if (event.target.classList.contains('edited_tweet_save')) {

        let tweetWrapper = event.target.parentElement.parentElement.parentElement,
            form = document.getElementById('editTweetForm'),
            tweetId = tweetWrapper.dataset.tweetId;

        handleTweetUpdate(form, tweetWrapper, tweetId);
    }
});
