import {renderDataFromUrlInsideBlock} from "./../functions/render/renderDataFromUrlInsideBlock.js";
import {updateFavorites} from "./../favorites/list";

document.addEventListener('click', function (event) {

    // Manage user subscriptions
    if (event.target.classList.contains('follow-user-button')) {
        let followUserWrapper = event.target.parentElement,
            userId = event.target.dataset.userId,
            dataAttribute = '[data-user-id="' + userId + '"]',
            buttonNodeList = document.querySelectorAll(dataAttribute),
            url = '/follow/user/' + userId;

        renderDataFromUrlInsideBlock(url, followUserWrapper, buttonNodeList);
        updateFavorites();
    }

    // Manage tweet likes
    if (event.target.classList.contains('like-tweet-button')) {
        let favoriteTweetWrapper = event.target.parentElement,
            tweetId = event.target.dataset.tweetId,
            url = '/manage/like/tweet/' + tweetId;

        renderDataFromUrlInsideBlock(url, favoriteTweetWrapper);
        updateFavorites();
    }

    // Manage favorite tweets
    if (event.target.classList.contains('favorite-tweet-button')) {
        let favoriteTweetWrapper = event.target.parentElement,
            tweetId = event.target.dataset.tweetId,
            url = '/manage/favorite/tweet/' + tweetId;

        renderDataFromUrlInsideBlock(url, favoriteTweetWrapper);
        updateFavorites();
    }

    // Manage favorite users
    if (event.target.classList.contains('favorite-user-button')) {
        let favoriteTweetWrapper = event.target.parentElement,
            favoriteUserId = event.target.dataset.favoriteUserId,
            dataAttribute = '[data-favorite-user-id="' + favoriteUserId + '"]',
            buttonNodeList = document.querySelectorAll(dataAttribute),
            url = '/manage/favorite/user/' + favoriteUserId;

        renderDataFromUrlInsideBlock(url, favoriteTweetWrapper, buttonNodeList);
        updateFavorites();
    }
});
